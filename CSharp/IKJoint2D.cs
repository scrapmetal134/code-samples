﻿using System.Collections.Generic;
using UnityEngine;

public class IKJoint2D : MonoBehaviour
{

    public int iterations = 0;
    [Range(0.01f, 1)]
    public float dampening;
    public Transform targetTransform;
    public Transform endTransform;
    public IKNode[] ikNodes = new IKNode[0];

    private Dictionary<Transform, IKNode> ikNodeCache;

    void Start()
    {
        ikNodeCache = new Dictionary<Transform, IKNode>(ikNodes.Length);
        foreach (var ikNode in ikNodes)
        {
            if (!ikNodeCache.ContainsKey(ikNode.nodeTransform))
            {
                ikNodeCache.Add(ikNode.nodeTransform, ikNode);
            }
        }
    }

    void LateUpdate()
    {

        if (targetTransform == null || endTransform == null)
        {
            return;
        }

        int i = 0;
        while (i < iterations)
        {
            CalculateIK();
            i++;
        }

        endTransform.rotation = targetTransform.rotation;
    }

    void CalculateIK()
    {
        Transform ikNodeTransform = endTransform.parent;
        while (true)
        {
            RotateTowardsTarget(ikNodeTransform);

            if (ikNodeTransform == transform)
            {
                break;
            }

            ikNodeTransform = ikNodeTransform.parent;
        }
    }

    void RotateTowardsTarget(Transform transform)
    {
        Vector2 distanceToTarget = targetTransform.position - transform.position;
        Vector2 distanceToEnd = endTransform.position - transform.position;
        float angle = SignedAngle(distanceToEnd, distanceToTarget);

        angle *= dampening;
        angle = -(angle - transform.eulerAngles.z);

        if (ikNodeCache.ContainsKey(transform))
        {
            var ikNode = ikNodeCache[transform];
            float parentRotation = transform.parent ? transform.parent.eulerAngles.z : 0;
            angle -= parentRotation;
            angle = ClampAngle(angle, ikNode.minAngle, ikNode.maxAngle);
            angle += parentRotation;
        }

        transform.rotation = Quaternion.Euler(0, 0, angle);
    }

    float SignedAngle(Vector3 a, Vector3 b)
    {
        float angle = Vector3.Angle(a, b);
        float sign = Mathf.Sign(Vector3.Dot(Vector3.back, Vector3.Cross(a, b)));

        return angle * sign;
    }

    float ClampAngle(float angle, float min, float max)
    {
        angle = Mathf.Abs((angle % 360) + 360) % 360;
        return Mathf.Clamp(angle, min, max);
    }
}

[System.Serializable]
public class IKNode
{
    public Transform nodeTransform;
    public float minAngle;
    public float maxAngle;
}
