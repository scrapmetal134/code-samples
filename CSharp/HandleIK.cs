﻿using UnityEngine;

public class HandleIK : MonoBehaviour
{
    public Animator anim;
    public float offsetY = 0f;
    public Transform leftFoot;
    public Transform rightFoot;

    private Vector3 leftFootPos;
    private Vector3 rightFootPos;
    private Quaternion leftFootRot;
    private Quaternion rightFootRot;
    private float leftFootWeight;
    private float rightFootWeight;
    private LayerMask layerMask;

    void Start()
    {
        leftFoot = anim.GetBoneTransform(HumanBodyBones.LeftFoot);
        rightFoot = anim.GetBoneTransform(HumanBodyBones.RightFoot);

        leftFootRot = leftFoot.rotation;
        rightFootRot = rightFoot.rotation;

        layerMask = ~(1 << LayerMask.NameToLayer("Ignore IK"));
    }

    void Update()
    {
        RaycastHit leftHit;
        RaycastHit rightHit;
        Vector3 leftPos = leftFoot.TransformPoint(Vector3.zero);
        Vector3 rightPos = rightFoot.TransformPoint(Vector3.zero);

        if (Physics.Raycast(leftPos, Vector3.down, out leftHit, 1))
        {
            leftFootPos = leftHit.point;
            leftFootRot = Quaternion.FromToRotation(transform.up, leftHit.normal) * transform.rotation;
        }

        if (Physics.Raycast(rightPos, Vector3.down, out rightHit, 1))
        {
            rightFootPos = rightHit.point;
            rightFootRot = Quaternion.FromToRotation(transform.up, rightHit.normal) * transform.rotation;
        }
    }

    void OnAnimatorIK()
    {
        leftFootWeight = anim.GetFloat("LeftFoot");
        rightFootWeight = anim.GetFloat("RightFoot");

        anim.SetIKPositionWeight(AvatarIKGoal.LeftFoot, leftFootWeight);
        anim.SetIKPositionWeight(AvatarIKGoal.RightFoot, rightFootWeight);

        anim.SetIKPosition(AvatarIKGoal.LeftFoot, leftFootPos + new Vector3(0, offsetY, 0));
        anim.SetIKPosition(AvatarIKGoal.RightFoot, rightFootPos + new Vector3(0, offsetY, 0));

        anim.SetIKRotationWeight(AvatarIKGoal.LeftFoot, leftFootWeight);
        anim.SetIKRotationWeight(AvatarIKGoal.RightFoot, rightFootWeight);

        anim.SetIKRotation(AvatarIKGoal.LeftFoot, leftFootRot);
        anim.SetIKRotation(AvatarIKGoal.RightFoot, rightFootRot);
    }
}
