﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(CapsuleCollider))]
[RequireComponent(typeof(Animator))]
public class PlayerCharacterController : MonoBehaviour
{
    [SerializeField]
    public float jumpPower;
    [Range(1f, 4f)]
    [SerializeField]
    public float gravityMultiplier;
    [SerializeField]
    public float groundCheckDistance;
    public Vector3 groundNormal;
    public bool shouldAnimateTurn;

    private bool isGrounded;
    private float origGroundCheckDistance;
    private float rotationAmount;
    private Rigidbody playerRigidbody;
    private Animator playerAnimator;
    private CapsuleCollider playerCapsule;

    #region monobehaviour
    void Start()
    {
        playerRigidbody = GetComponent<Rigidbody>();
        playerCapsule = GetComponent<CapsuleCollider>();
        playerAnimator = GetComponent<Animator>();

        // Constrain ridigidbody rotation
        playerRigidbody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;

        origGroundCheckDistance = groundCheckDistance;
    }
    #endregion

    #region public methods
    /// <summary>
    /// Apply movement, jump and crouch to the character
    /// </summary>
    /// <param name="move">Movement vector for moving the character</param>
    /// <param name="crouch">Whether or not the character should be crouching</param>
    /// <param name="jump">Whether or not the character should be jumping</param>
    public void ApplyMovement(Vector3 move, bool crouch, bool jump)
    {
        if (move.magnitude > 1f)
        {
            move.Normalize();
        }

        UpdateGroundStatus();

        if (isGrounded)
        {
            HandleGroundMovement(crouch, jump);
        }
        else
        {
            HandleAirMovement();
        }

        UpdateAnimator(move);
    }

    /// <summary>
    /// Apply Rotation to the character
    /// </summary>
    /// <param name="turn">The amount the character should turn</param>
    public void ApplyRotation(float turn)
    {
        rotationAmount += turn;
        transform.localEulerAngles = new Vector3(
            transform.localEulerAngles.x,
            rotationAmount,
            transform.localEulerAngles.z
        );

        if (shouldAnimateTurn)
        {
            playerAnimator.SetFloat("Side", turn * 0.85f, 0.1f, Time.deltaTime);
        }
    }
    #endregion

    #region private methods

    /// <summary>
    /// Update the animator with the current move vector
    /// </summary>
    /// <param name="move">Current move vector</param>
    private void UpdateAnimator(Vector3 move)
    {
        playerAnimator.SetFloat("Forward", move.z, 0.1f, Time.deltaTime);
        playerAnimator.SetFloat("Side", move.x, 0.1f, Time.deltaTime);
        playerAnimator.SetBool("isGrounded", isGrounded);
    }

    /// <summary>
    /// Handle movement while the character is in air
    /// </summary>
    private void HandleAirMovement()
    {
        Vector3 extraGravityForce = (Physics.gravity * gravityMultiplier) - Physics.gravity;
        playerRigidbody.AddForce(extraGravityForce);

        groundCheckDistance = playerRigidbody.velocity.y < 0 ? origGroundCheckDistance : 1f;
    }

    /// <summary>
    /// Handle movement when the character is on the ground
    /// </summary>
    /// <param name="crouch">Whether or not the character should be jumping</param>
    /// <param name="jump">Whether or not the character should be crouching</param>
    private void HandleGroundMovement(bool crouch, bool jump)
    {
        bool isAnimGrounded = playerAnimator.GetCurrentAnimatorStateInfo(0).IsName("Grounded");

        if (jump && !crouch && isAnimGrounded)
        {
            playerRigidbody.velocity = new Vector3(playerRigidbody.velocity.x, jumpPower, playerRigidbody.velocity.z);
            isGrounded = false;
            playerAnimator.applyRootMotion = false;
            groundCheckDistance = 1f;
        }
    }

    /// <summary>
    /// Check to see if we are grounded
    /// </summary>
    private void UpdateGroundStatus()
    {
        RaycastHit hitInfo;

        if (Physics.SphereCast(transform.position + (Vector3.up * 0.6f), (playerCapsule.radius * 0.3f), Vector3.down, out hitInfo, groundCheckDistance))
        {
            groundNormal = hitInfo.normal;
            isGrounded = true;
            playerAnimator.applyRootMotion = true;
        }
        else
        {
            isGrounded = false;
            groundNormal = Vector3.up;
            playerAnimator.applyRootMotion = false;
        }
    }
    #endregion
}
