﻿Shader "Sprites/PaletteSwapSprite"
{
	Properties
	{
		_MainTex ("Sprite Texture", 2D) = "white" {}
		[MaterialToggle] PixelSnap ("PixelSnap", Float) = 0
	}

	SubShader
	{
		Tags { 
			"Queue"="Transparent" 
			"IgnoreProjector"="True" 
			"RenderType"="Transparent" 
			"PreviewType"="Plane"
			"CanUseSpriteAtlas"="True"
			}
		Cull Off 
		ZWrite Off 
		Blend One OneMinusSrcAlpha

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma multi_compile _ PIXELSNAP_ON
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 color    : COLOR;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 color    : COLOR;
				float4 vertex : SV_POSITION;
			};


			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.color = v.color;
				#ifdef PIXELSNAP_ON
				o.vertex = UnityPixelSnap(o.vertex);
				#endif

				return o;
			}
			
			sampler2D _MainTex;
			sampler2D _PaletteTexture;

			fixed4 frag (v2f i) : SV_Target
			{
				fixed4 orgColor = tex2D (_MainTex, i.uv);
				orgColor *= i.color;
				fixed4 newColor = tex2D (_PaletteTexture, float2(orgColor.r, 0));
				newColor.rgb *= orgColor.a;
				newColor.a = orgColor.a;
				return newColor;
			}
			ENDCG
		}
	}
}